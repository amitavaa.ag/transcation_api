class Response
  def initialize(status: nil, success: nil, errors: [], data: {})
    @status = status
    @errors = errors
    @success = success
    @data = data
  end

  def self.success(status: :ok, data: {})
    new(status: status, success: 'ok', data: data)
  end

  def self.failure(status: nil, errors: [])
    new(status: status, success: 'fail', errors: errors)
  end

  def as_json(options)
    super(only: []).merge(status: @status, success: @success, data: @data)
  end
end

