class AddIndexInTransactions < ActiveRecord::Migration[6.0]
  def change
    add_index :transactions, :parent_id, where: "(parent_id IS NOT NULL)"
  end
end
