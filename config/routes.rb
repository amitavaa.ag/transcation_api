Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  # sidekiq ui shit
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  scope module: :api, path: :api do
    resources :transactions, path: :transactionservice, only: [] do
      post 'transactions', on: :collection, action: 'create'
      put 'transactions/:id', on: :collection, action: 'update'
      get 'transactions/:id', on: :collection, action: 'show'
      get 'types', on: :collection, action: 'types'
    end
  end
end

