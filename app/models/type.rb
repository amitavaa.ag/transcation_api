class Type < ApplicationRecord
  has_many :transactions

  before_validation :downcase_name

  validates :name, uniqueness: true

  private

  def downcase_name
    self.name = name.downcase
  end
end
