require 'rails_helper'

RSpec.describe Type, type: :model, unit: true do
  let(:name) { 'Test' }

  it 'downcases type name before save' do
    type = Type.new(name: name)
    type.save

    type.reload
    
    expect(type.name).to eq(name.downcase)
  end

  it 'raises error on duplicate entry' do
    Type.create!(name: name)

    t = Type.new(name: name).save
    expect(t).to be false
  end
end
