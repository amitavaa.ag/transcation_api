class Calculate
  def initialize(transaction, depth)
    @transaction = transaction
    @depth = depth
  end

  def total
    amount = TransactionCache.get(@transaction)

    return { amount: amount.to_f, cached: true } if amount.present?

    amount = if @transaction.parent.present?
               sum_with_parents(@transaction)
             else
               @transaction.amount
             end

    { amount: amount, cached: false }
  end

  private

  def sum_with_parents(transaction)
    counter = 0
    sum = 0


    until iterable?(transaction, counter)
      amount_from_cache = TransactionCache.get(transaction)

      return sum + amount_from_cache.to_i if amount_from_cache.present?

      sum += transaction.amount

      counter += 1
      transaction = transaction.parent
    end

    sum
  end

  def iterable?(transaction, counter)
    transaction.nil? || counter >= @depth
  end
end

