class CreateTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :transactions do |t|
      t.numeric :amount, precision: 100, scale: 2, null: false
      t.references :type, foreign_key: true, null: false, index: true
      t.bigint :parent_id
    end
  end
end
