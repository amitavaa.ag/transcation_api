class Transaction < ApplicationRecord
  include QueryRunner

  belongs_to :type

  # attribute :id, :bigint
  # attribute :type_id, :bigint, foreign_key: types.id
  # attribute :amount, :numeric, precision: 100, scale: 2
  # attribute :parent_id, :bigint, null: true
  # attribute :accumulated_total, :decimal

  validates :amount, numericality: { greater_than_or_equal_to: 0 }
  validates :type, presence: true

  def parent
    Transaction.find_by(id: parent_id)
  end

  def self_and_descendants(depth = 0)
    return [self] unless parent.present? || depth >= Constants::MAX_DEPTH

    [self] | parent.self_and_descendants(depth + 1).flatten
  end

  def _children
    childs = Transaction.where(parent_id: id)
    return [] unless childs.present?

    childs | childs.map(&:children).flatten
  end

  def children
    res = Transaction.execute_raw(Transaction.childrens_query(self))
    return [] unless res.present?

    res.reject { |val| val['id'] == id }.map { |v| Transaction.instantiate(v) }
  end

  def total
    value = Transaction.execute_raw!(Transaction.descendants_query(self))
    value.first[:total].to_f
  end

  private

  class << self
    def childrens_query(transaction)
      cttable = 'cte'

      rec_sql = <<~SQL
        SELECT
          trs.id,
          trs.parent_id,
          trs.amount
          --ts.amount::DECIMAL as accumulated_total
        FROM
          transactions trs, cte
        WHERE
          trs.parent_id = #{cttable}.id
      SQL

      CteQuery.new(
        table: cttable,
        context_stmt: Transaction.where(id: transaction.id).select(:id, :parent_id, :amount).to_sql,
        recurse_stmt: rec_sql,
        select_stmt: 'SELECT cte.id, cte.parent_id, cte.amount as total FROM cte'
      ).query
    end

    def descendants_query(transaction)
      cttable = 'cte'
      depth_col = 'rdepth'

      rec_sql = <<~SQL
        SELECT
          trs.id,
          trs.parent_id,
          trs.amount,
          #{cttable}.#{depth_col} + 1
        FROM
          transactions trs, cte
        WHERE
          trs.id = #{cttable}.parent_id AND #{cttable}.rdepth < #{Constants::MAX_DEPTH}
      SQL

      CteQuery.new(
        table: cttable,
        context_stmt: Transaction.where(id: transaction.id).select(:id, :parent_id, :amount, "0 as #{depth_col}").to_sql,
        recurse_stmt: rec_sql,
        select_stmt: "SELECT SUM(#{cttable}.amount) as total FROM cte"
      ).query
    end
  end

  def iterable?(transaction, counter)
    transaction.nil? || counter >= Constants::MAX_DEPTH
  end

end

