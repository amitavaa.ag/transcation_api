class Errors::QueryBuilder < StandardError
  def initialize(msg='query builder failed')
    @code = 100
    @type = self.class.name.downcase.to_sym

    super(msg)
  end
end
