class CalculateSumWorker
  include Sidekiq::Worker
  sidekiq_options queue: 'calculate_sum_queue'

  def perform(opts)
    transaction = Transaction.find(opts['transaction_id'])

    TransactionCache.evict(transaction) if opts['evict']

    calculator = Calculate.new(transaction, Constants::MAX_DEPTH)
    total = calculator.total

    TransactionCache.set(transaction, total[:amount])
  end
end

