Figaro.require_keys(
  'database_name',
  'database_host',
  'database_user',
  'database_port',
  'database_password',
  'redis_url'
)
