require 'rails_helper'

RSpec.describe 'Transactions', type: :request do
  let(:headers) { { "Content-Type": 'application/json' } }

  describe 'POST transactions#update' do
    before(:each) { TransactionCache.flush }
    after(:each) { TransactionCache.flush }

    context 'success' do
      context 'type is not present' do
        it 'should create a new type along with transaction' do
          req_params = {
            amount: 1000,
            type: 'car'
          }.to_json

          post '/api/transactionservice/transactions', params: req_params, headers: headers

          transaction = Transaction.last

          expect(CalculateSumWorker).to have_enqueued_sidekiq_job({
            'transaction_id' => transaction.id,
            'evict' => false
          })
          expect(json_body[:success]).to eq('ok')

          Sidekiq::Worker.drain_all

          resp = TransactionCache.get(transaction)
          expect(resp.to_f).to eq(1000)
        end
      end

      context 'failure' do
        it 'should fail if amount is negative' do
          req_params = {
            amount: -1000,
            type: 'car'
          }.to_json

          post '/api/transactionservice/transactions', params: req_params, headers: headers

          expect(json_body[:success]).to eq('fail')
          expect(json_body[:status]).to eq('bad_request')
        end
      end
    end
  end


  describe 'GET transactions#show' do
    context 'success' do
      it 'should return the parent sum when no descendants are present' do
        trx = create_transaction(amount: 5000)

        get "/api/transactionservice/transactions/#{trx.id}", headers: headers

        expect(json_body[:data][:sum].to_f).to eq(5000.to_f)
      end

      it 'returns the cummulative sum when there are multiple parents' do
        trx1 = create_transaction(amount: 5000)
        trx2 = create_transaction(amount: 4000, parent_id: trx1.id)

        Sidekiq::Worker.drain_all

        get "/api/transactionservice/transactions/#{trx2.id}", headers: headers

        expect(json_body[:data][:sum].to_f).to eq(9000.to_f)

        cached_trx1 = TransactionCache.get(trx1)
        cached_trx2 = TransactionCache.get(trx2)

        expect(cached_trx1.to_f).to eq(5000)
        expect(cached_trx2.to_f).to eq(9000)
      end
    end
  end

  describe 'PUT transactions#update' do
    context 'success' do
      it 'evicts the cache when amount or parent_id changes' do
        trx1 = create_transaction(amount: 20)
        trx2 = create_transaction(amount: 30, parent_id: trx1.id)
        trx3 = create_transaction(amount: 40, parent_id: trx2.id)
        trx4 = create_transaction(amount: 50, parent_id: trx3.id)

        Sidekiq::Worker.drain_all

        expect(TransactionCache.get(trx3).to_f).to eq(90)

        put "/api/transactionservice/transactions/#{trx2.id}", headers: headers, params: { amount: 70, type: 'car' }.to_json

        expect(json_body[:success]).to eq('ok')

        Sidekiq::Worker.drain_all

        expect(TransactionCache.get(trx3)).to eq(nil)
      end
    end
  end
end
