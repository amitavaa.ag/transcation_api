class TransactionCache
  class << self
    def get(transaction)
      amount = TRANSACTION_CACHE.with do |redis|
        redis.get(key(transaction.id))
      end

      amount
    end

    def set(transaction, value)
      TRANSACTION_CACHE.with do |redis|
        redis.watch(key(transaction.id)) { redis.set(key(transaction.id), value) }
      end
    end

    def evict(transaction)
      keys = transaction.children.pluck(:id).map{ |id| key(id) }
      return unless keys.present?

      TRANSACTION_CACHE.with do |redis|
        redis.del(*keys)
      end
    end

    def flush
      TRANSACTION_CACHE.with(&:flushall)
    end

    private

    def key(suffix)
      "#{Constants::TRX_CACHE_KEY}#{suffix}"
    end
  end
end

