require 'rails_helper'

RSpec.describe Transaction, type: :model, unit: true do
  let(:type) { Type.create(name: 'bird') }

  context 'validations' do
    it 'returns error is amount is less than 0' do
      transaction = Transaction.new(amount: -100, type_id: type.id)

      expect(transaction.valid?).to be false
      expect(transaction.errors).to include('amount')
    end

    it 'returns error if type id is not present' do
      transaction = Transaction.new(amount: 100, type_id: 1)

      expect(transaction.valid?).to be false
      expect(transaction.errors).to include('type')
    end
  end

  context '#self_and_descendants' do
    let(:transaction) { Transaction.new(amount: 100, type: type) }

    it 'returns true if no parent id is present' do
      transaction.save!

      expect(transaction.self_and_descendants.map(&:id)).to eq([transaction.id])
    end

    it 'return false if parent is present' do
    end
  end

  it 'returns its transaction amount when no parent exists' do
    t = Transaction.create(amount: 100, type: type)
    expect(t.amount).to eq(100)
  end

  it 'calculates the total sum of transactions of parents' do
    parent_id = nil
    total = 0

    6.times do |i|
      t = Transaction.create(amount: i * 10, type: type, parent_id: parent_id)
      parent_id = t.id

      total += i * 10
    end

    total = Transaction.last.total
    expect(total).to eq(total)
  end
end

