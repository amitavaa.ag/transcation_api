#!/bin/bash

# Shortcut to booting up app and sidekiq worker
# usage bash run.sh app|specs|sidekiq [--skip-config]
# usage
# run.sh 

function debug {
    debug_mode=${DEBUG}
    [[ "$debug_mode" == "debug" ]] && echo "$@"
}


function copy_config() {
    debug "copying config $1 to config/application.yml"

    cp "$1" config/application.yml 
}


function setup_local() {
    debug "installing rails and bundler"

    gem install bundler
    gem install rails

    debug "installing gems for app"

    bundle install --binstubs
}


function docker_run_app {
    docker-compose run web bundle exec rails db:create
    docker-compose run web bundle exec rails db:migrate
    docker-compose up --build
}

function docker_run_spec {
    docker-compose run web bundle exec rails db:create RAILS_ENV=test
    docker-compose run web bundle exec rails db:migrate RAILS_ENV=test
    docker-compose run web bundle exec rspec spec/

}


while getopts ":h" opt; do
  case ${opt} in
    h )
        echo "usage:"
        echo "    run.sh local | docker                # boots up locally or in docker."
        echo "options:"
        echo "    -c                                   # sample config file to copy to config/application.yml"
        echo "    -c  config/application.yml.sample"
        echo "    -w                                   # boots up sidekiq worker"
        echo "    -t                                   # runs tests"
        echo "    -s                                   # boots up app"
        echo "docker options:"
      ;;
    \? )
        echo "Usage: cmd [-h]"
        exit 1
      ;;
  esac
done

shift $((OPTIND -1))

subcommand=$1; shift  # Remove executable name from arg list



case "$subcommand" in
    local  )
        config_file=""
        sidekiq_start=0
        run_tests=0
        start_app=0

        while getopts "c:wths" sopt; do
            case ${sopt} in
                c )
                    config_file=${OPTARG}
                    copy_config "$config_file"
                    ;;
                s )
                    start_app=1
                    ;;
                w )
                    sidekiq_start=1
                    ;;
                t )
                    run_tests=1
                    ;;
                h )
                    echo "./run.sh local -c [=config_sample] -w [worker] -s [server] -t [tests] -h [help]"
                    exit 0
                    ;;
                : )
                    echo "invalid option ${OPTARG}"
                    echo "Usage: cmd [-h]"
                    exit 1
                    ;;
                ? )
                    echo "invalid options ${OPTARG}"
                    echo "Usage: cmd [-h]"
                    exit 1
                    ;;
            esac
        done

        setup_local

        if [[ ${run_tests} -eq 1 ]]; then
            ./bin/rspec spec/
        fi

        if [[ ${sidekiq_start} -eq 1 && ${start_app} -eq 1 ]]; then
            echo "sidekiq and app can not boot at the same time"
            exit 1
        fi

        if [[ ${start_app} -eq 1 ]]; then
            ./bin/rake db:create
            ./bin/rake db:migrate
            ./bin/rails s
        elif [[ ${sidekiq_start} -eq 1 ]]; then
            ./bin/sidekiq -c config/sidekiq.yml
        fi

        shift $((OPTIND -1))
        ;;
    docker )
        config_file=""
        sidekiq_start=0
        run_tests=0
        start_app=0

        while getopts "c:wths" sopt; do
            case ${sopt} in
                c )
                    config_file=${OPTARG}
                    copy_config "$config_file"
                    ;;
                s )
                    start_app=1
                    ;;
                w )
                    sidekiq_start=1
                    ;;
                t )
                    run_tests=1
                    ;;
                h )
                    echo "./run.sh docker -c [=config_sample] -w [worker] -s [server] -t [tests] -h [help]"
                    exit 0
                    ;;

                : )
                    echo "invalid option ${OPTARG}"
                    echo "Usage: cmd [-h]"
                    exit 1
                    ;;
                ? )
                    echo "invalid options ${OPTARG}"
                    echo "Usage: cmd [-h]"
                    exit 1
                    ;;
            esac
        done

        docker-compose build


        if [[ ${run_tests} -eq 1 ]]; then
            docker_run_spec
        fi

        if [[ ${sidekiq_start} -eq 1 ]]; then
            docker-compose run sidekiq
        elif [[ ${start_app} -eq 1 ]]; then
            docker_run_app
        fi

        shift $((OPTIND -1))
        ;;
    \? )
          echo "invalid option: -$OPTARG" 1>&2
          exit 1
          ;;
    : )
          echo "invalid option: -$OPTARG requires an argument" 1>&2
          exit 1
          ;;
esac

