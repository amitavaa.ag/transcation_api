class CteQuery
  def initialize(table: 'cte', context_stmt: '', select_stmt: '', recurse_stmt: '')
    @table = table

    @context_stmt = context_stmt
    @recurse_stmt = recurse_stmt
    @select_stmt  = select_stmt
  end

  def query
    validate

    @query ||= [with_query, '(', clauses, ')', @select_stmt].join("\n")
  end

  def to_s
    @query
  end

  private

  def with_query
    return "WITH RECURSIVE #{@table} AS" if recursive?

    "WITH #{@table} AS"
  end

  def clauses
    return @context_stmt unless recursive?

    [@context_stmt, ' UNION ALL ', @recurse_stmt].join("\n")
  end

  def recursive?
    @recurse_stmt.present?
  end

  def validate
    raise Errors::QueryBuilder.new("SELECT statement is missing") unless @select_stmt.present?
  end
end
