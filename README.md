# README

### Requirements

- Ruby ~> 2.7
- Rails ~> 5
- Postgres ~> 12.0
- Redis

Or Just have docker and docker-compose


### Setup
- For docker
    - ./run.sh docker -c config/application.yml.sample -s
    - ./run.sh docker -w
- For local
    - ./run.sh local -c config/application.yml.sample -s
    - ./run.sh local -w

- ./run.sh -h # for more opts 
- ./run.sh local|docker -t # for tests

Howver in case local run fails:
- gem install bundler
- gem install rails
- bundle install --binstubs
- ./bin/rake db:create db:migrate
- ./bin/rails s # in one terminal
- ./bin/sidekiq -c config/sidekiq.yml # in another terminal

### Routes

```
                   Prefix Verb URI Pattern                                        Controller#Action
              sidekiq_web      /sidekiq                                           Sidekiq::Web
transactions_transactions POST /api/transactionservice/transactions(.:format)     api/transactions#create
                          PUT  /api/transactionservice/transactions/:id(.:format) api/transactions#update
                          GET  /api/transactionservice/transactions/:id(.:format) api/transactions#show
       types_transactions GET  /api/transactionservice/types(.:format)            api/transactions#types
```


### How it works

- Cache stores the sum of the `self.amount + parents.map(&:amount)`

- GET Api tries to get the sum of the present transactions and its descendants
  - It tries to look up the value in the cache, if not fetches from db, and moves up the descendants
    until a cache is hit, at which point it returns the cumulate sum

- POST api is straight forward, it insert a record in the db and fires a background worker to update the cache

- PUT api first updates the database and then evicts the cache for all the child elements, same as in insert

- The background workers should ideally be reading from a postgres read replica, which I was unable to setup in docker-compose

### Drawbacks

- This is a very read heavy system, for every PUT call, getting all the children is Costly operation.
- The eviction of cache leads to future db calls for the coming GET Apis, until eventually the Cache is full
- There will be a possibilty of serving stale data in case of a highly concurrent system, which could be solved via a better eviction policy, but not TTL
- The re-population of cache can probably be done by a cron job or another background worker via throttling to reduce the load, but it hard to say how the system would react.

Assuming that the GET to PUT request ratio is 5:1 Its safe to say around 2 to 3 times the data will be served from cache. 

The idea of being able to update the parent_id makes it a bit hard. One can cache the siblings and parents in a redis SET , but then they again need to be evicated on case basis, meaning if the parent_id has changed.
Which can be a good optimization depending of if the parent_id change is less frequent.


