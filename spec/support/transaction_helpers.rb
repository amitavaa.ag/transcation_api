module TransactionHelpers
  def create_transaction(amount: 1000, type: 'car', parent_id: nil)
    headers = { "Content-Type": 'application/json' }
    req_params = {
      amount: amount,
      type: type,
      parent_id: parent_id
    }.to_json

    post '/api/transactionservice/transactions', params: req_params, headers: headers

    Transaction.last
  end
end

RSpec.configure do |config|
  config.include TransactionHelpers, type: :request
end
