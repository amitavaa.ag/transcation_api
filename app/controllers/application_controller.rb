class ApplicationController < ActionController::API
  rescue_from StandardError,  with: :internal_server_error

  rescue_from ActiveRecord::RecordNotFound, with: :not_found

  rescue_from ActionController::ParameterMissing,
    ActiveModel::UnknownAttributeError,
    ActiveModel::ValidationError,
    ActiveRecord::RecordInvalid,
    ActiveRecord::RecordNotUnique,
    with: :bad_request

  def ping
    render json: {}, status: :ok
  end


  private

  def internal_server_error(error)
    render_error(:internal_server_error, error, 'Something went wrong')
  end

  def not_found(error)
    render_error(:not_found,error, 'Resource was not found')
  end

  def bad_request(error)
    render_error(:bad_request, error, 'Invalid request')
  end

  def render_error(status, error, message)
    # Statsd Metric Send
    # Log errors
    puts error.to_json if enable_logging?
    render json: Response.failure(status: status, errors: [message]), status: status
  end

  def enable_logging?
    %w[development test].include?(Figaro.env.rails_env.downcase)
  end
end
