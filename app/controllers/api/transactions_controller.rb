class Api::TransactionsController < ::ApplicationController
  def create
    transaction_params = create_transaction_params.merge(type: transaction_type)
    
    transaction = Transaction.new(transaction_params)
    transaction.save!

    CalculateSumWorker.perform_async({
      'transaction_id' => transaction.id,
      'evict' =>false
    })

    render json: Response.success(status: :created), status: :created
  end

  def update
    transaction = Transaction.find(transaction_id)
    prev_type   = transaction.type

    transaction_params = update_transaction_params.merge(type: transaction_type)
    transaction.update!(transaction_params)

    CalculateSumWorker.perform_async({
      'transaction_id' => transaction.id,
      'evict' => true
    })
    

    render json: { success: 'ok' }, status: :ok
  end

  def show
    transaction = Transaction.find(transaction_id)
    total = transaction.total

    TransactionCache.set(transaction, total)

    render json: Response.success(data: { sum: total }), status: :ok
  end

  def types
    trx_ids = Type.find_by(name: params.fetch(:name).downcase).transactions.pluck(:id)
    render json: Response.success(data: trx_ids), status: :ok
  end

  private

  def create_transaction_params
    params.permit(:amount, :type, :parent_id)
  end

  def update_transaction_params
    params.permit(:amount, :type, :parent_id, :id)
  end

  def type_params
    params.fetch(:type)
  end

  def parent_id
    params.fetch(:parent_id, nil)
  end

  def transaction_id
    params.fetch(:id)
  end

  def transaction_type
    @transaction_type ||= Type.find_or_create_by!(name: type_params.downcase)
  end
end
