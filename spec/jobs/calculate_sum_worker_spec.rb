require 'rails_helper'

RSpec.describe CalculateSumWorker, unit: true do
  let(:calculator) { double(Calculate) }
  let(:transaction) { double(Transaction) }

  describe '#perform' do
    it 'computes the sum from database and sets it in cache' do
      transaction_id = 2
      depth = Constants::MAX_DEPTH

      allow(transaction).to receive(:children).and_return([])
      allow(calculator).to receive(:total).and_return({ amount: 1000, cached: false })
      expect(Transaction).to receive(:find).with(transaction_id).and_return(transaction)
      expect(Calculate).to receive(:new).with(transaction, depth).and_return(calculator)

      expect(TransactionCache).to receive(:set).and_return(true)

      CalculateSumWorker.new.perform({
        'transaction_id' => transaction_id, 'evict' => false
      })
    end
  end
end
