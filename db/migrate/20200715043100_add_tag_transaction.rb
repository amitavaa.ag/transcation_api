class AddTagTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :transactions, :tag, :string
    reversible do |way|
      way.up { change_column :transactions, :amount, :decimal }
      way.down { change_column :transactions, :amount, :numeric, scale: 2, precision: 100 }
    end
  end
end
