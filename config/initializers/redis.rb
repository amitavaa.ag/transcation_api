def with_sentinels(sentinel_hosts)
  return {} unless sentinel_hosts.present?

  sentinels = sentinel_hosts.split(',').map do |str|
    host, port = str.strip.split(':')
    { host: host, port: port }
  end

  return {} unless sentinels.present?

  { sentinels: sentinels, role: 'master' }
end

TRANSACTION_CACHE = ConnectionPool.new(size: Figaro.env.cache_conn_pool_size.to_i, timeout: Figaro.env.cache_timeout_in_sec.to_i) do
  options =  { url: Figaro.env.redis_url }

  password = Figaro.env.redis_password
  options[:password] = password if password.present?

  # off the form x.x.x.x:23679, y.y.y.y:23679
  sentinel_hosts = Figaro.env.redis_sentinels
  options = options.merge(with_sentinels(sentinel_hosts))

  Redis.new(options)
end


## SideKiq Config
redis_conn = proc {
  Redis.new(url: Figaro.env.sidekiq_cache_url)
}

Sidekiq.configure_client do |config|
  config.redis = ConnectionPool.new(size: 5, &redis_conn)
  config.average_scheduled_poll_interval = 2
end

Sidekiq.configure_server do |config|
  config.redis = ConnectionPool.new(size: 25, &redis_conn)
end

Sidekiq.default_worker_options = { backtrace: true }
