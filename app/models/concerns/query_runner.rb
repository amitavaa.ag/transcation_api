module QueryRunner
  extend ActiveSupport::Concern

  class_methods do
    def execute_raw!(query)
      @result = self.connection.execute(query)
      raise ActiveRecord::RecordNotFound unless exists?

      values
    end

    def execute_raw(query)
      @result = self.connection.execute(query)
      return [] unless exists?

      values
    end

    def values
      fields = @result.fields
      @result.values.map { |value| Hash[fields.zip(value)].with_indifferent_access }
    end

    def exists?
      return false unless @result.present?

      @result.values&.first&.first.present?
    end
  end
end
