require 'rails_helper'

RSpec.describe Calculate, unit: true do
  let(:transaction) { double(Transaction) }

  context 'when sum not in redis' do
    before do
      allow(transaction).to receive(:id).and_return(2).exactly(3).times
      allow(transaction).to receive(:parent).and_return(nil)
    end

    it 'calculates the total sum of transactions of parents' do
      expect(transaction).to receive(:amount).and_return(100)

      calculate = Calculate.new(transaction, 10)
      expect(calculate.total[:amount]).to eq(100)
    end
  end

  context 'when sum is in redis' do
    before { TransactionCache.flush }
    after { TransactionCache.flush }

    it 'does not call model methods from db and returns from cache' do
      allow(transaction).to receive(:id).and_return(2).exactly(3).times
      TransactionCache.set(transaction, 300)

      expect_any_instance_of(Transaction).not_to receive(:amount)
      expect(TransactionCache).to receive(:get).and_call_original

      calculate = Calculate.new(transaction, 10)
      total = calculate.total
      expect(total[:amount]).to eq(300.0)
      expect(total[:cached]).to eq(true)
    end

    it 'uses the memoized value of sum from cache' do
      t2 = double(Transaction)
      t3 = double(Transaction)

      allow(t3).to receive(:id).and_return(3)
      allow(t3).to receive(:amount).and_return(200)
      allow(t3).to receive(:parent).and_return(t2)

      expect(t2).to receive(:id).and_return(2).exactly(3).times
      expect(t2).not_to receive(:amount)

      TransactionCache.set(t2, 100)

      calc = Calculate.new(t3, 10)
      expect(calc.total[:amount]).to eq(300)
    end
  end
end

